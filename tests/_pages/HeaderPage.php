<?php

class HeaderPage
{
    // include url of current page
    static $URL = '/';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */


    static $element_contactNumber = '//*[@id="content-container"]/div[1]/div/div[1]/div/h4';
    static $value_contactNumber = '1-800-571-2596';
    static $element_contactSlider = '//*[@id="content-container"]/div[1]/div/ul[1]/li[1]/span';
    static $element_emailSlider = '//*[@id="content-container"]/div[1]/div/ul[1]/li[3]/span';
    static $element_emailSuccessMessage = '//*[@id="content-container"]/div[1]/div/div[2]/div[1]/div[1]/div/ul/li/ul/li/span';
    static $element_quoteSlider = '//*[@id="content-container"]/div[1]/div/ul[1]/li[4]/span';
    static $element_Login = '//*[@id="content-container"]/div[1]/div/ul[2]/li[2]/a';

    static $element_topHeaderPageContainer = '.top-header';
    static $element_contactSlideToggle = '.contact-slide-toggle';
    static $element_emailSlideToggle = '.email-slide-toggle';
    static $element_rquoteSlideToggle = '.rquote-slide-toggle';
    static $element_topAccountLinkSlideToggle = '.top-account-links';


    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: EditPage::route('/123-post');
     */
     public static function route($param)
     {
        return static::$URL.$param;
     }


}