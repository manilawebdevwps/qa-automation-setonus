<?php

class SearchPage
{
    // include url of current page
    static $URL = '/';

    static $placeholder_search = '.rtcurver';
    static $element_search = '#search';
    static $element_go = '.btn-go';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: EditPage::route('/123-post');
     */
     public static function route($param)
     {
        return static::$URL.$param;
     }


}