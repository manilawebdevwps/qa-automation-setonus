<?php

class LoginPage
{
    // include url of current page
    static $URL = '/customer/account/login/';

    static $usernameField = '#email';
    static $passwordField = '#pass';
    static $loginButton = "#send2";

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: EditPage::route('/123-post');
     */
     public static function route($param)
     {
        return static::$URL.$param;
     }


}

//*[@id="content-container"]/div[1]/div/div[2]/div[1]/div[2]/div[2]/h5