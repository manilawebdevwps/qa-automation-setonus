<?php
namespace Codeception\Module;

// here you can define custom functions for WebGuy 

class WebHelper extends \Codeception\Module
{
    public function waitForUserInput(){
        if(trim(fgets(fopen("php://stdin","r")))!= chr(13)) return;
    }
    public function seeLoginLink(){
        try {
            $this->getModule('WebDriver')->click('Login');
        }catch (\PHPUnit_Framework_AssertionFailedError $e){
            $this->getModule('WebDriver')->click('Logout');
        }
        return true;
    }
    public function getArrayFromDB($table, $column, $criteria = array()){
        $dbh = $this->getModule('Db');
        $query = $dbh->driver->select($column, $table,$criteria);
        $dbh->debugSection('Query', $query, json_encode($criteria));

        $sth = $dbh->driver->getDbh()->prepare($query);
        if (!$sth) \PHPUNIT_Framework_Assert::fail("Query '$query' can't be executed.");

        $sth->execute(array_values($criteria));
        return $sth->fetchAll();
    }

    public function getArrayFromDBRandom($table, $column, $criteria = array(), $count){
        $dbh = $this->getModule('Db');
        $query = $dbh->driver->select($column, $table,$criteria);
        //$query = 'select * from category1';
        if($count>0){
            $query.='ORDER BY RAND() LIMIT '.$count;
        }
        $dbh->debugSection('Query', $query, json_encode($criteria));

        $sth = $dbh->driver->getDbh()->prepare($query);
        if (!$sth) \PHPUNIT_Framework_Assert::fail("Query '$query' can't be executed.");

        $sth->execute(array_values($criteria));
        return $sth->fetchAll();
    }

    public function getArrayFromDBOrderByWithLimit($table, $column, $criteria = array(), $order, $count=0){
        $dbh = $this->getModule('Db');
        $query = $dbh->driver->select($column, $table,$criteria);
        //$query = 'select * from category1';
        $query.=' ORDER BY '.$order;
        echo $query;
        if($count>0){
            $query.=' LIMIT '.$count;
        }
        //echo $query;
        $dbh->debugSection('Query', $query, json_encode($criteria));

        $sth = $dbh->driver->getDbh()->prepare($query);
        if (!$sth) \PHPUNIT_Framework_Assert::fail("Query '$query' can't be executed.");

        $sth->execute(array_values($criteria));
        return $sth->fetchAll();
    }
    public function changeBaseURL($url){
        $this->getModule('WebDriver')->_reconfigure(array('url' => $url));
    }
    function seeCanCheckEverything($thing)
    {

        $var = $this->assertTrue(isset($thing), "this thing is set");
        return $var;
//        $this->assertFalse(empty($any), "this thing is not empty");
//        $this->assertNotNull($thing, "this thing is not null");
//        $this->assertContains("world", $thing, "this thing contains 'world'");
//        $this->assertNotContains("bye", $thing, "this thing doesn`t contain 'bye'");
//        $this->assertEquals("hello world", $thing, "this thing is 'Hello world'!");
    }
    public function assertLink($Locator){
        try {
            $this->getModule('WebDriver')->seeLink($Locator);
        }catch (\PHPUnit_Framework_AssertionFailedError $e){
            return false;
        }
        return true;
    }

}
