<?php
namespace uat;
use \WebGuy;
use \Codeception\Util\Locator;

class MWSD1066Cest
{

    public function _before()
    {

    }

    public function _after()
    {
    }
    public static $element_spring = 'html/body/div[1]/div[6]/div/ul/li[1]/div/a';
    public static $element_pipemarker = 'html/body/div[1]/div[4]/div/ul/li[2]/div/a';
    public static $element_safety = 'html/body/div[1]/div[6]/div/ul/li[3]/div/a';

    function _check404page(WebGuy $I){
        $I->wantTo('Want to check if the website encounter 404 page');
        $I->maximizeWindow();
        $I->amOnPage('/preview.html');
        $I->cantSee('We are sorry, but the page you are looking for cannot be found.');
//        $I->waitForUserInput();
    }
    function _TestPathAndHoverOnMenu(WebGuy $I, $category){
        $I->moveMouseOver('.nav-left-home > li');
        $I->moveMouseOver(".nav-left-home > li:nth-child(".$category.")");
        $I->waitForElementVisible('.level1-menu:hover > .sub-menu-home');
    }
    function _TestPathAndHoverOnMenuForInnerPages(WebGuy $I, $category){
        $I->moveMouseOver('.nav-1');
        $I->waitForElementVisible('.nav-left-inner');
        $I->expectTo("Element = .nav-left-inner > li:nth-child(".$category.")");
        $I->moveMouseOver(".nav-left-inner > li:nth-child(".$category.")");
        //$I->waitForElementVisible('.level1-menu:hover > .sub-menu');
    }
    // tests
    public function NavContainer(WebGuy $I) {
        $this->_check404page($I);
        //$I->wantTo('To check Navigation Container');
        $I->expectTo('See Level 1 Menu Category when nav container is hovered');
        $I->canSee('Shop by Category','.nav-1 > span');

    }
    public function CategoryLevel1(WebGuy $I) {

//        $I->amOnPage('/');
        //$this->_check404page($I);
        $I->wantTo('To check Level 1 Category Navigation');
        $I->expectTo('See Level 1 Category');

        $results = $I->getArrayFromDBOrderByWithLimit('category1','*',array(),'rank');
        foreach ($results as $key=>$val) {
            $I->expectTo('See Level 1 Category '.$val['name']);
            //$I->assertLink(Locator::href('/'.$val['link']));
            $I->moveMouseOver(Locator::href('/'.$val['link']));
            $I->canSeeLink($val['name'],$val['link']);
            //$I->canSeeElement(Locator::href('/'.$val['link']));
        }
    }
    public function CategoryLevel2(WebGuy $I) {

//        $this->_check404page($I);
        $I->wantTo('To check Level 2 Category Navigation');
        $I->expectTo('See Level 2 Category when Level 1 Category when hovered');

        $results_lvl1 = $I->getArrayFromDBOrderByWithLimit('category1','*',array(),'rank');
        foreach ($results_lvl1 as $key=>$val) {
            $this->_TestPathAndHoverOnMenu($I,$val['id']);
            $results = $I->getArrayFromDBOrderByWithLimit('category2','*',array('category1_id' => $val['id']),'rank');
            $I->wait(4);

            foreach ($results as $key2=>$val2) {
                $I->expectTo('See Level 2 Category = '.$val2['name']);
                $I->canSeeLink($val2['name'],$val2['link']);

            }// query for Level 2

        }// query for Level 1

    }

    public function CategoryLevel3(WebGuy $I) {

//        $this->_check404page($I);
//        $scenario->skip('');
        $I->wantTo('To check Level 3 Category Navigation');
        $I->expectTo('See Level 3 Category when Level 1 Category when hovered');

        $results_lvl1 = $I->getArrayFromDBOrderByWithLimit('category1','*',array(),'rank');
        foreach ($results_lvl1 as $key=>$val) {
            $this->_TestPathAndHoverOnMenu($I,$val['id']);
            $results_lvl2 = $I->getArrayFromDBOrderByWithLimit('category2','*',array('category1_id' => $val['id']),'rank');

            foreach ($results_lvl2 as $key2=>$val2) {
                $results_lvl3 = $I->getArrayFromDBOrderByWithLimit('category3','*',array('category2_id' => $val2['id']),'rank');
                $I->wait(.5);

                foreach($results_lvl3 as $key3=>$val3){
                    $I->expectTo('See Level 3 Category = '.$val3['name'].'with Level 2 Category id = '.$val2['id'].' and with Parent Category '.$val['name']);
                    $I->canSeeLink($val3['name'],$val3['link']);

                }// query for Level 3

            }// query for Level 2

        }// query for Level 1
    }
    public function InnerPages(WebGuy $I){
        $I->wantTo('Check Navigation on Inner Pages');
        $I->amOnPage('/preview-inner.html');
        $I->expectTo('See Level 1 Category Page');

        $random_lvl1 = $I->getArrayFromDBRandom('category1','*',array(),1);
        foreach($random_lvl1 as $key=>$val){
            $I->wait(1);
            $this->_TestPathAndHoverOnMenuForInnerPages($I,$val['id']);
            $I->click($val['name']);
            $I->canSeeInCurrentUrl($val['link']);
            $I->expectTo('See Level 1 Header on the actual page');
            $I->waitForElementVisible('.inner h1');
            $I->canSee($val['name'],'.inner h1');

            $random_lvl2 = $I->getArrayFromDBRandom('category2','*',array('category1_id' => $val['id']),1);
            $I->amOnPage('/preview-inner.html');
            foreach($random_lvl2 as $key2=>$val2){
                $this->_TestPathAndHoverOnMenuForInnerPages($I,$val['id']);
                $I->wait(1);
                $I->click($val2['name']);
                $I->canSeeInCurrentUrl($val2['link']);
                $I->expectTo('See Level 2 Header on the actual page');
                $I->waitForElementVisible('.inner h1');
                $I->canSee($val2['name'],'.inner h1');

                $random_lvl3 = $I->getArrayFromDBRandom('category3','*',array('category2_id' => $val2['id']),1);
                $I->amOnPage('/preview-inner.html');
                foreach($random_lvl3 as $key3=>$val3){
                    $this->_TestPathAndHoverOnMenuForInnerPages($I,$val['id']);
                    $I->wait(1);
                    $I->click($val3['name']);
                    $I->canSeeInCurrentUrl($val3['link']);
                    $I->expectTo('See Level 3 Header on the actual page');
                    $I->waitForElementVisible('.inner h1');
                    $I->canSee($val3['name'],'.inner h1');
                    $I->amOnPage('/preview-inner.html');
                }// query for Level 3 random

            }// query for Level 2 random

        }// query for Level 1 random
    }

    public function CarouselContainer(WebGuy $I) {
        $I->amOnPage('/preview.html');
        $I->wantTo('See Carousel');
        $I->canSee('Carousel');
    }

    public function CarouselSliders(WebGuy $I) {
        $I->amOnPage('/preview.html');
        $I->wantTo('See Carousel Sliders and it\'s Links');
        $I->expectTo('See 3 Slide Links');
        //$I->wait(30);
        $I->click('Spring Cleanup');
        $I->click('Pipe Marking Solutions');
        $I->click('Safety Made Easy');
    }

    public function CarouselSliderElements(WebGuy $I){
        $I->amOnPage('/preview.html');
        $I->wantToTest('If all of the Links for each Sliders are present and working');
        $I->click('Spring Cleanup');
        $I->canSeeLink('Shop Now');
        $I->click('Shop Now');
        $I->canSeeInCurrentUrl('/promotions/spring-clean-up-kits.html');
        $I->wait(5);
        //Pipe Marking Solution
        $I->amOnPage('/preview.html');
        $I->click('Pipe Marking Solutions');
        $I->canSeeLink('Shop Now');
        $I->click(self::$element_pipemarker);
        $I->wait(5);
        $I->canSeeInCurrentUrl('/pipe-markers/pipe-marking-101/');
        //Safety Made Easy
        $I->amOnPage('/preview.html');
        $I->click('Safety Made Easy');
        $I->canSeeLink('Click here to learn more');
        $I->click('Click here to learn more');
        $I->canSeeInCurrentUrl('/safety-made-easy');
        //banner
//        $I->canSee('.offer-banner');
    }

}