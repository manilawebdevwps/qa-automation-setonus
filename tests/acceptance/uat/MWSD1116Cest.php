<?php
namespace UAT;
use \WebGuy;

class MWSD1116Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }
	
    // tests
    public function CheckRemovedOrderBy(WebGuy $I) {

    $I->wantTo('If Order by badge is removed on the simple product page');
	$I->amOnPage('/polished-firefly-exit-signs-86875.html');
    $I->dontsee('Order in the next');
    }
}