<?php
namespace UAT;
use \WebGuy;

class MWSD960Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function AddNoFollowOnBestPages(WebGuy $I) {
    $I->wantToTest('If all Best Pages inserted the meta tag');
    $I->expectTo('See Best Page');
    $I->canSeeInCurrentUrl('/pages');
    $I->expectTo('Meta tag with robot name');
    $I->canSeeElementInDOM('robots','meta');
    }
}