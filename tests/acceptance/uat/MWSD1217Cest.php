<?php
namespace uat;
use \WebGuy;

class MWSD1217Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function RemoveIndexFromWarrantyLink(WebGuy $I) {
    $I->amOnPage('/labels-decals/osha-safety/osha.html');
    $I->wantToTest('If the index.php is remove from source code of warranty.php');
    $I->expectTo('not to see index.php on warranty');
    $I->cantSee('http://www.seton.com/index.php/warranty.php');
    }

}