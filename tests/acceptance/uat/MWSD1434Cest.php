<?php
namespace uat;
use \WebGuy;

class MWSD1434Cest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function LivePersonChat(WebGuy $I) {
    $I->amOnPage('/');
    $I->wantToTest('LivePerson Chat');
    $I->canSee('.chat-icon');
    $I->clickWithRightButton('//*[@id="content-container"]/div[1]/div/ul[1]/li[2]');
    $I->expectTo('LivePerson when I click Live Chat in the header and in My Account section');
    }

}