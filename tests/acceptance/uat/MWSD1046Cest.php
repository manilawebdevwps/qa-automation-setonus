<?php
namespace uat;
use \WebGuy;
use \Codeception\Util\Locator;
/**
 * @guy WebGuy\UserSteps
 */

class MWSD1066Cest
{

    public function _before()
    {

    }

    public function _after()
    {
    }

    // tests
    public function HeaderContainer(WebGuy\UserSteps $I) {
        $I->wantTo('To check Header Container');
        $I->check404page();
        $I->expectTo('See Level 1 Header');
        $I->canSee('Signs','h1');
        $I->expectTo('See Category Banner');
        $I->canSee('.category-banner');
        $I->expectTo('See Sign Finder');
        $I->canSee('.sign-finder');
        $I->expectTo('See Category List View');
        $I->canSee('.category-view-max');

    }

    public function CategoryLinks(WebGuy $I) {

        $I->wantTo('To check Sub Category Links');
        $I->expectTo('See Level 2 Category Links');

        $results_lvl1 = $I->getArrayFromDBOrderByWithLimit('category1','*',array(),'rank');
        foreach ($results_lvl1 as $key=>$val) {
            $results = $I->getArrayFromDBOrderByWithLimit('category2','*',array('category1_id' => $val['id']),'rank');
            foreach ($results as $key2=>$val2) {
                $I->expectTo('See Level 2 Category = '.$val2['name']);
                $I->canSeeLink($val2['name'],$val2['link']);
                $results_lvl3 = $I->getArrayFromDBOrderByWithLimit('category3','*',array('category2_id' => $val2['id']),'rank');
                foreach($results_lvl3 as $key3=>$val3){
                    $I->expectTo('See Level 3 Category = '.$val3['name'].'with Level 2 Category id = '.$val2['id'].' and with Parent Category '.$val['name']);
                    $I->canSeeLink($val3['name'],$val3['link']);

                }// query for Level 3

            }// query for Level 2

        }// query for Level 1

    }
}