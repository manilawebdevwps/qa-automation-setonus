<?php
use \WebGuy;
/**
 * @guy WebGuy\UserSteps
 */

class SearchCest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function SearchDisplay(WebGuy $I) {
        $I->amOnPage(SearchPage::$URL);
        $I->wantTo('See Search Bar and Go Button is displayed');
        $I->seeElement(SearchPage::$placeholder_search);
        $I->seeElement(SearchPage::$element_go);
    }

    public function SearchLinkEmpty(WebGuy\UserSteps $I) {
        $I->amOnPage(SearchPage::$URL);
        $I->wantTo('Click GO with empty search keyword');
        $I->clickSearch('');
        $I->expectTo('See a Pop-up message');
        $I->acceptPopup();
//        $I->waitForUserInput();

    }
}