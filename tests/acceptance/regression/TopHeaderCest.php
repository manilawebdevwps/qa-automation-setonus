<?php
use \WebGuy;
/**
 * @guy WebGuy\UserSteps
 */

class TopHeaderPageCest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function TopHeaderPageDisplay(WebGuy $I) {
        $I->amOnPage(HeaderPage::$URL);
        $I->wantTo('To check top-HeaderPage class is present');
        $I->seeElement(HeaderPage::$element_topHeaderPageContainer);
        $I->expectTo('See all listed menu');
        $I->seeElement(HeaderPage::$element_contactSlideToggle);
        $I->seeElement(HeaderPage::$element_emailSlideToggle);
        $I->seeElement(HeaderPage::$element_rquoteSlideToggle);
        $I->seeElement(HeaderPage::$element_topAccountLinkSlideToggle);
    }

    public function TopHeaderPageCall(WebGuy $I){
        $I->amOnPage(HeaderPage::$URL);
        $I->wantTo('To check Contact slider is displayed');
        $I->click(HeaderPage::$element_contactSlider);
        $I->expectTo('See Contact Number in the Contact slide');
        $I->moveMouseOver(HeaderPage::$element_contactSlider);
        $I->see(HeaderPage::$value_contactNumber,HeaderPage::$element_contactNumber);
        $I->expectTo('See Contact slider toggling');
        $I->click(HeaderPage::$element_contactSlider);

    }

    public function TopHeaderPageChat(WebGuy\UserSteps $I){
        $I->amOnPage(HeaderPage::$URL);
        $I->wantTo('To check Pop-up chat is displayed');
        $I->click('Live Chat');
        $I->switchToWindow('custclient');
        $I->seeInCurrentUrl('/customer_service_webchat_customer_form.htm');
        $I->wantTo('To check if input will lead to the result page.');
        $I->liveChat();
        $I->seeInCurrentUrl('/client');
    }

    public function TopHeaderPageSignup(WebGuy\UserSteps $I){
        $I->amOnPage(HeaderPage::$URL);
        $I->wantTo('To check if Email Sign-up slider displayed');
        $I->click(HeaderPage::$element_emailSlider);
        $I->expectTo('See of confirmation message will after submitting');
        $I->signUp();
        $I->waitForElementVisible(HeaderPage::$element_emailSuccessMessage, 10);
    }

    public function TopHeaderPageRequestAQuote(WebGuy $I){
        $I->amOnPage(HeaderPage::$URL);
        $I->wantTo('Check if Request a quote slider displayed');
        $I->click(HeaderPage::$element_quoteSlider);
    }

    public function TopHeaderPageLoginLink(WebGuy\UserSteps $I){
        $I->amOnPage(HeaderPage::$URL);
        $I->wantTo('Link to Login Page');
        $I->seeLoginLink();
        $I->login('bradyqatest@gmail.com','bradyqatest');


    }

    public function TopHeaderPageRegisterLink(WebGuy $I){
        $I->amOnPage(HeaderPage::$URL);
        $I->wantTo('Links to Register Page');
        $I->click('Register');

    }

}
