<?php
use \WebGuy;

class NavigationCest
{

    public function _before()
    {
    }

    public function _after()
    {
    }

    // tests
    public function MainNavigationDisplay(WebGuy $I) {
        $I->amOnPage('/');
        $I->wantTo('Check 10 Level 1 is displayed');
//        $I->seeElement('html body#-body.cms-index-index div#content-container div.wrapper div.nav-container ul#nav li.level0 a.level-top span');
        $results = $I->getArrayFromDB('category1','*',array());
        foreach ($results as $key=>$val) {
            $I->expectTo('See Level 1 Category '.$val['keyword']);
            $I->seeElement($val['ui_locator']);

        }

    }

}