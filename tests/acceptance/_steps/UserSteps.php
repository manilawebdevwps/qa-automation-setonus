<?php
namespace WebGuy;

use Faker\Factory;

class UserSteps extends \WebGuy
{

    function login($name, $password)
    {
        $I = $this;
        $I->amOnPage(\LoginPage::$URL);
        $I->fillField(\LoginPage::$usernameField, $name);
        $I->fillField(\LoginPage::$passwordField, $password);
        $I->click(\LoginPage::$loginButton);
    }
    //Sign Up for Mailing List in Top Header Navigator
    function signUp(){
        $I = $this;
        $faker = Factory::create();
        $I->fillField('subscriber_firstname',$faker->firstName);
        $I->fillField('subscriber_lastname',$faker->lastName);
        $I->fillField('email_su',$faker->email);
        $I->click('.es-form-btn');
    }

    function liveChat(){
        $I = $this;
        $faker = Factory::create();
        $I->fillField('fname', $faker->firstName);
        $I->fillField('optionaldata5', $faker->email);
        $I->fillField('optionaldata4','Sample Question for Automation');
        $I->click('Submit');
    }

    function clickSearch($keyword){
        $I = $this;
        $I->fillField(\SearchPage::$element_search, $keyword);
        $I->click(\SearchPage::$element_go);
    }

    function check404page($url= '/'){
        $I = $this;
        $I->wantTo('Want to check if the website encounter 404 page');
        $I->amOnPage($url);
        $I->cantSee('We are sorry, but the page you are looking for cannot be found.');
    }

}